# Guestbook Deployment and Monitoring

[Guestbook](https://github.com/kubernetes/examples/tree/master/guestbook) is a simple, multi-tier PHP-based web application that uses redis chart.

This repo, provides a single stop solution for monitoring, log analysis and CD.

## Introduction

This chart bootstraps a [guestbook](https://github.com/kubernetes/examples/tree/master/guestbook) deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

It also packages the [Bitnami Redis chart](https://github.com/kubernetes/charts/tree/master/stable/redis) which is required for bootstrapping a Redis deployment for the caching requirements of the guestbook application.

## Prerequisites
* Google account (GCP)
* `kubectl` and `helm` packages installed on the machine trying out this deployment.

## Steps

Follow the quick start to create a [GKE cluster](https://cloud.google.com/kubernetes-engine/docs/quickstart)

* Follow the same the link to sync the `kubeconfig` to access the cluster


### Installing the Chart

> **Tip**: This is manual step, you can use CD to deploy this chart

To install the chart with the release name `my-release`:

```console
cd helm-charts/
$ helm install --name my-release .
```

The command deploys the guestbook on the Kubernetes cluster in the default configuration. The [configuration](#configuration) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

#### Uninstalling the Chart
```
helm del --purge my-release
```
### Continuous deployment:

* This repo has `.gitlab-ci.yml` file which will be used to deploy the helm chart on the kubernetes cluser under `development` namespace.

#### Setting up cluster for CD:

* Follow the documentation to add the [cluster](https://docs.gitlab.com/ee/user/project/clusters/#adding-an-existing-kubernetes-cluster) gitlab-ci

### Setting up Monitoring:

* Clone the repo
* Assuming the user has setup the `kubeconfig`

> **Tip**: verify the access to the cluster by `kubectl get nodes`

* Apply the monitoring configuration to `kubernetes` cluster
```
kubectl apply -f monitoring.yaml
```

* The above command will set up `namespace`, `prometheus`, `alertmanager`, `grafana` along with few basic rules for the monitoring

* This will setup `monitoring` namespace

* Grafana has default `prometheus` dashboard and a `custom dashboard` which visualizes the cluster, node, pod metrics
* To access the grafana
```
kubectl get service grafana -n monitoring -o jsonpath="{.status.loadBalancer.ingress[0].ip}"
```
* Login credentials to access grafana dashboards
```
Username: admin
# To get the password
kubectl get secret --namespace monitoring grafana -o jsonpath="{.data.admin-password}" | base64 -D
```

### Log analysis

* In the repo, There is a helm chart to deploy `elastic search`, `filebeat` and `grafana`

* To deploy the helm chart

```
cd efk-stack
helm dep build .
helm install . -n <Release name> --namespace <Namespace in Kubernetes>
```
* Kibana is not publically accessible because it doesnt have any authentication. To access the kibana

```
kubectl port-forward <POD_NAME> 5601 -n <Namespace>
```

### Script for manual deployment

* Usage:
```
./deploy.sh -l <Region in GCP > -p <Project ID> -c <Zone in GCP>
```

