#!/bin/bash
set -ex
usage() {
  # this function prints a usage summary, optionally printing an error message
  local ec=0

  if [ $# -ge 2 ] ; then
    # if printing an error message, the first arg is the exit code, and
    # all remaining args are the message.
    ec="$1" ; shift
    printf "====\nERROR: %s\n\n" "$*" >&2
  fi

  cat <<EOF
Usage:
       $(basename $0) [-h | -p <PROJECT_ID> | -l <LOCATION> | -c <COMPUTE_ZONE>]

Pre-Reqs:
* gcloud binary installed
* Make sure that you are logged in to GCP, if not login by gcloud login
* Get the project ID where you would like to deploy the cluster gcloud projects list

Required arguments:

  -p <PROJECT_ID>       Project ID in GCP
  -l <LOCATION>         Region where the cluster needs to be deployed
  -c <COMPUTE_ZONE>     Compute zone where the cluster needs to be deployed
  
Options

  -h              This help message.
EOF

  exit $ec
}

if [[ -z "$1" ]]; then
    usage 1 "must specify arguments."
fi


# Initialise variables
PROJECT_ID=''
LOCATION=''
COMPUTE_ZONE=''

# -r, -l, -b and -d require an arg. -h don't. 
while getopts ':hp:l:c:' opt; do
  case "$opt" in 
    p) PROJECT_ID="$OPTARG" ;;
    l) LOCATION="$OPTARG" ;;
    c) COMPUTE_ZONE="$OPTARG" ;;

    h) usage ;;
    :) usage 1 "-$OPTARG requires an argument" ;;
    ?) usage 1 "Unknown option \"$opt\"" ;;
  esac
done

if [[ -z "$PROJECT_ID" ]]; then
    usage 1 "Must specify resource group with -p"
fi
if [[ -z "$LOCATION" ]]; then
    usage 1 "Must specify azure dns zone location with -l"
fi
if [[ -z "$COMPUTE_ZONE" ]]; then
    usage 1 "Must specify base resource group -c"
fi

# Variables


MY_KUBERNETES_CLUSTER=guest-book
HELM_CHART_NAME=guest-book
DEP_NAMESPACE=development


# Set GCP project ID
gcloud config set project ${PROJECT_ID}
gcloud config set compute/zone ${COMPUTE_ZONE}

gcloud container clusters create ${MY_KUBERNETES_CLUSTER} --zone ${COMPUTE_ZONE} --num-nodes=1

gcloud container clusters get-credentials ${MY_KUBERNETES_CLUSTER} --region ${COMPUTE_ZONE}

# Creating necessary permissions for Helm and tiller
kubectl create serviceaccount -n kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account tiller

# sleep till the tiller pod is ready.
echo "Sleeping 60 till the tiller pod is ready"
sleep 60

# deploying guest-book helm chart

cd helm-charts/
helm dep build .
helm install --name ${HELM_CHART_NAME} --namespace ${DEP_NAMESPACE} .

# Monitoring stack deployment
# Namespace: monitoring
cd ..
kubectl apply -f monitoring.yaml
GRAFANA_URL=$(kubectl get service grafana -n monitoring -o jsonpath="{.status.loadBalancer.ingress[0].ip}")
echo "GRAFANA URL: ${GRAFANA_URL}"

# EFK Stack deployment
# Namespace: efk
cd efk-stack
helm dep build .
helm install . -n efk --namespace efk

GUESTBOOK_URL=$(kubectl get service ${HELM_CHART_NAME}-guestbook -n ${DEP_NAMESPACE} -o jsonpath="{.status.loadBalancer.ingress[0].ip}")
echo "GUEST_BOOK URL: ${GUESTBOOK_URL}"
GRAFANA_URL=$(kubectl get service grafana -n monitoring -o jsonpath="{.status.loadBalancer.ingress[0].ip}")
echo "GRAFANA URL: ${GRAFANA_URL}"


